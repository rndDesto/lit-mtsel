import {defineConfig} from 'vite'
import * as path from 'path';

export default defineConfig({
    build: {
      lib: {
        entry: 'src/index.ts',
        formats: ['es', 'cjs'],
      },
      // generate manifest.json in outDir
      rollupOptions: {
        // overwrite default .html entry
        external:[/^lit/],
      },
    },
  })