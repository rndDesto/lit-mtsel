import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'

/**
 * An example element.
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
@customElement('heading-lit')
export default class HeadingLit extends LitElement {
  /**
   * Copy for the read the docs hint.
   */
  @property({type: String})
  title = 'Welcome here.'


  render() {
    return html`
      <div class="root">
      Ini Heading ${this.title}
      </div>
    `
  }

  static styles = css`
    .root{
        background:green
    }
  `
}

declare global {
  interface HTMLElementTagNameMap {
    'heading-lit': HeadingLit
  }
}
