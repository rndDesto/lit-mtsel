import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'

/**
 * An example element.
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
@customElement('button-lit')
export default class ButtonLit extends LitElement {
  /**
   * Copy for the read the docs hint.
   */
  @property({type: String})
  label = 'Ini Button'


  render() {
    return html`
      <div class="root" @click=${this._onClick}>
      ${this.label}
      <slot></slot>  
      </div>
    `
  }

  private _onClick() {
    console.log("button clicked!")
  }

  static styles = css`
    .root{
        background:red
    }
  `
}

declare global {
  interface HTMLElementTagNameMap {
    'button-lit': ButtonLit
  }
}
